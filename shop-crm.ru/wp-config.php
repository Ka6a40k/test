<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'eBr5;Z=o.)Y#@yUK8-,E<u}y1<]$b!hr)nZ5JI/1h:vJGBp(?,K*n*%py.|?QoY&');
define('SECURE_AUTH_KEY',  ']%y/Sa3*z75_{o58+ME>0qXC[6&5p9FXf<qQ12Cyxqf[]8ukcN6nlg-9@DOGS&Hi');
define('LOGGED_IN_KEY',    '<xgc.{:o,o<iI;qG hrs_U)R$I!wq@A2.vP< O_=5N/pP[S^n~WQZM:w6CCscIx-');
define('NONCE_KEY',        'DVR&poTqvciPa.NuIJg#*L:PP^vM;>m_bFZ|TF`!ddFi}w5!OE>;)Vuq]11/)sak');
define('AUTH_SALT',        'Cd)ejGu&TJONfX/G$T]}en6@1K$BcG)Hl|;dRZiwUbY/y*e{-V~VGOUY:8qw<M3#');
define('SECURE_AUTH_SALT', 'vpU0T733]niHOXY6n^thFf4-qk-Z/<4K,*SR_q>G~Tz-LR@Af=+>TE&$oPUnu`+C');
define('LOGGED_IN_SALT',   '?7K_DgjZQrPCZVM_?}hSqDcHy5vl;}F,R8~dAT;N,f%;?[9|%MJV_Dh]8{csC Yy');
define('NONCE_SALT',       'jM o;ta~Xvncp3e~@wHLt?y2n87^cz[(HK,z1{&ACRP45  pv3gx)?%qwvSI_&*z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
