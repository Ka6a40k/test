<?php get_header();?>
<section class="our-service js-check" data-underline="main-f">
	<div class="container text-center">
		<h2 class="title">Наши <span>компетенции</span></h2>
		<hr class="under-title">
		<div class="services-container">
			<!--Вывод услуг-->
			<?php 
			$query1 = new WP_Query(array('post_type'=>'services'));

			while ( $query1->have_posts() ) : $query1->the_post(); ?>

					<div class="service-item col-md-4 col-sm-6">
						<a href="<?=the_permalink()?>" class="item-link">
							<div class="icon-box"><i 
							style="color: <?= types_render_field("icon-colorr", array('display'=>'raw')) ?>"
							class="<?= types_render_field("icon-clas", array('display'=>'raw')) ?>"></i></div>
							<div class="text-box"><?= the_title(); ?></div>
						</a>
					</div>
					
			<?php endwhile; ?>

			<!--END вывод услуг-->
		</div>
	</div>
</section>
<section class="about-us">

	<div class="container text-center">
		<h2 class="title"><span>T</span>echnology to <span>E</span>very <span>O</span>ne</h2>
		<hr class="under-title">
		<p class="desc">С 2011 года мы помогаем компаниям разрабатывать и внедрять программное обеспечение, которое выводит бизнес на новый качественный уровень. Мы – это команда экспертов, архитекторов, дизайнеров, разработчиков и аналитиков. Используя свои опыт и знания, мы создаем инновационные технологии для решения бизнес-задач.</p>
		<h3 class="caption">Почему стоит выбрать <span>нас</span>?</h3>
		<div class="wrapper">
			<div class="item col-md-3 col-sm-3 col-xs-6">
				<div class="box">
					<div class="icon-box"><i class="fa fa-key" aria-hidden="true"></i></div>
					<div class="text-box">Разработка CRM-системы под ключ</div>
				</div>
			</div>
			<div class="item col-md-3 col-sm-3 col-xs-6">
				<div class="box">
					<div class="icon-box"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
					<div class="text-box">Срок разработки 10 дней</div>
				</div>
			</div>
			<div class="item col-md-3 col-sm-3 col-xs-6">
				<div class="box">
					<div class="icon-box"><i class="fa fa-check" aria-hidden="true"></i></div>
					<div class="text-box">С гарантией результата</div>
				</div>
			</div>
			<div class="item col-md-3 col-sm-3 col-xs-6">
				<div class="box">
					<div class="icon-box"><i class="fa fa-star" aria-hidden="true"></i></div>
					<div class="text-box">Опыт работы более 5-и лет</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="ask">
	<div class="container text-center">
		<h2 class="title">К нам <span>обращаются</span> :</h2>
		<hr class="under-title">
		<div class="wrapper">
			<div class="item-s col-md-12">
				<div class="text-box col-md-8 col-xs-10">Когда необходимо разработать систему, полностью адаптированную под требования бизнеса</div>
				<div class="icon-box col-md-4 col-xs-2"><img src="<?=get_template_directory_uri(); ?>/img/icon-1.png" alt="" class="img-icon"></div>
			</div>
			<div class="item-s col-md-12">
				<div class="icon-box col-md-4 col-xs-2"><img src="<?=get_template_directory_uri(); ?>/img/icon-2.png" alt="" class="img-icon"></div>
				<div class="text-box col-md-8 col-xs-10">Для оптимизации времени и качества общения со своими клиентами</div>
			</div>
			<div class="item-s col-md-12">
				<div class="text-box col-md-8 col-xs-10">Чтобы повысить эффективность работы своих менеджеров с клиентами</div>
				<div class="icon-box col-md-4 col-xs-2"><img src="<?=get_template_directory_uri(); ?>/img/icon-3.png" alt="" class="img-icon"></div>
			</div>
		</div>
	</div>
</section>
<!-- <div class="container text-center">
<?php echo do_shortcode("[show-testimonials orderby='menu_order' order='ASC' layout='slider' options='transition:fade,adaptive:false,controls:pager,pause:4000,auto:on,columns:1,theme:speech,info-position:info-left,text-alignment:left,quote-content:short,charlimitextra: (...),display-image:on,image-size:ttshowcase_small,image-shape:circle,image-effect:none,image-link:on']"); ?>
</div> -->
<!-- <section class="partners">
	<div class="container text-center">
		<h2 class="title">Среди наших <span>клиентов</span> :</h2>
		<hr class="under-title">
		<div class="wrapper partners-slider">

			<?php 
			$query2 = new WP_Query(array('post_type'=>'partners'));
			while ( $query2->have_posts() ) : $query2->the_post(); ?>
					<div class="slider-item"><img src="<?= types_render_field("logo", array('output'=>'raw')) ?>" alt="" class="item-img"></div>
			<?php endwhile; ?>

		</div>
	</div>
</section> -->
<?php get_footer(); ?> 