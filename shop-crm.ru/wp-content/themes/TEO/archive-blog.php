<?php get_header(); ?>
<section class="blog-sec js-check" id="blog" data-underline="blog-f">
	<div class="container text-center">
		<h2 class="title">Блог <span>TEO</span> - слухи, мифы и лайфхаки <br> об автоматизации бизнеса</h2>
		<hr class="under-title-s">
		<div class="wrapper">
		<!--ВЫВОД ВСЕХ СТАТЕЙ-->
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $shortDesc = types_render_field('short-desc', array('output'=>'raw')); ?>
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<div class="item-box">
					<div class="item-body">
						<div class="img-box">
							<?php the_post_thumbnail( 'large', array('class' => "item-img")) ?>
						</div>
						<div class="text-box">
							<h3 class="item-title"><?= the_title(); ?></h3>
							<p class="item-desc"><?php 
							if(strlen($shortDesc) > 184) {
								 echo mb_substr($shortDesc, 0, 184) . '...';
							} else {
								echo $shortDesc;
							} ?>	
							</p>
						</div>
					</div>
					<div class="item-footer">
						<a href="<?= the_permalink(); ?>" class="footer-link">Читать статью</a>
					</div>
				</div>
			</div>

		<?php endwhile; else: ?>
		<p><?php _e('Записей пока нет...'); ?></p>
		<?php endif; ?>
		<!--END ВЫВОД ВСЕХ СТАТЕЙ-->
		</div>
		<?php the_posts_pagination(); ?>
	</div>
</section>

<?php get_footer(); ?>