<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<section class="page404" id="page404">
	<div class="container">
		<h2>404 ошибка</h2>
			<p>Вы попали на страницу которая отсутствует.</p>
			<p>Вы можете посетить другие разделы нашего сайта:</p>
		<ul class="navigate-box">
			<li class="nav-links" id="main-f"><div class="active-item"></div><a href="<?= home_url() ?>" class="nav-item">Главная</a></li>
			<li class="nav-links" id="services-f"><div class="active-item"></div><div class="active-item"></div><a href="<?= home_url() . '/services' ?>" class="nav-item">Услуги</a></li>
			<li class="nav-links" id="portfolio-f"><div class="active-item"></div><a href="<?= home_url() . '/portfolio' ?>" class="nav-item">Портфолио</a></li>
			<li class="nav-links" id="shares-f"><div class="active-item"></div><a href="<?= home_url() . '/shares' ?>" class="nav-item">Акции от партнеров</a></li>
			<li class="nav-links" id="blog-f"><div class="active-item"></div><a href="<?= home_url() . '/blog' ?>" class="nav-item">Блог</a></li>
			<li class="nav-links" id="contacts-f"><div class="active-item"></div><a href="<?= home_url() . '/contacts' ?>" class="nav-item">Контакты</a></li>
		</ul>
		<p>Просим извинить нас за предоставленные неудобства.</p>
	</div>
</section>
<?php get_footer(); ?>
