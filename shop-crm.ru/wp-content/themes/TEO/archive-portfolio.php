<?php get_header(); ?>

<section class="portfolio-sec js-check" data-underline="portfolio-f" id="portfolio">
		<div class="container text-center">
			<h2 class="title">Портфолио</h2>
			<hr class="under-title-s">
			<p class="desc">Нами реализовано более <?= wp_count_posts()->publish ?> проектов, представляем вам самые интересные с нашей точки зрения</p>
			<div class="wrapper">
			
				<!--ВЫВОД РАБОТ -->
				<?php query_posts($query_string.'&posts_per_page=100');
				if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="item col-md-3 col-sm-6 col-xs-12">
						<a href="<?= the_permalink() ?>" class="item-link">
							<div class="img-box">
								<?php the_post_thumbnail( 'medium', array(
									'class' => "item-img",
								)) ?>
							</div>
							<div class="text-box">
								<p class="item-desc"><?= the_title();?></p>
							</div>
							<?php if(types_render_field('favorite_work', array('output'=>'raw')) == 1): ?>
								<div class="best-work-box">Лучшая работа!</div>
							<?php endif; ?>
						</a>
					</div>
				<?php endwhile; else: ?>
				<p><?php _e('Записей пока нет...'); ?></p>
				<?php endif; ?>
				<!--END ВЫВОД РАБОТ -->
				
				<!--ПУСТАЯ РАБОТА -->
				<div class="item empty-work col-md-3 col-sm-6 col-xs-12">
					<a href="" class="item-link">
						<div class="img-box">
							<img src="<?=get_template_directory_uri(); ?>/img/your-work.png" alt="" class="item-img">
						</div>
						<div class="text-box">
							<p class="item-desc">Мы оставили место для вашего заказа :) </p>
						</div>
					</a>
				</div>
				<!-- END ПУСТАЯ РАБОТА -->
				
			</div>
		</div>
</section>

<?php get_footer(); ?>