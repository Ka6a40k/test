<?php get_header(); ?>

<section class="our-clients js-check" data-underline="shares-f" id="clients">
	<div class="container text-center">
		<h2 class="title">Акции от партнеров</h2>
		<hr class="under-title-s">
		<p class="desc">Специально для наших клиентов</p>
		<div class="wrapper">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $shortDesc = types_render_field('stock-desc', array('output'=>'raw')); ?>
			<div class="item col-md-4 col-xs-12 col-sm-4">
				<div class="item-box">
				<?php if(types_render_field('best-offer', array('output'=>'raw')) == 1): ?>
					<div class="best-offer">Лучшее предложение</div>
				<?php endif; ?>
					<div class="item-body">
						<div class="img-box">
							<img src="<?= types_render_field('stock-img', array('output'=>'raw')) ?>" alt="" class="item-img">
						</div>
						<div class="text-box">
							<h3 class="item-title"><?= the_title(); ?></h3>
							<p class="item-desc"><?php 
							if(strlen($shortDesc) > 184) {
								 echo mb_substr($shortDesc, 0, 184) . '...';
							} else {
								echo $shortDesc;
							} ?>	
							</p>
						</div>
					</div>
					<div class="item-footer">
						<a target="_blank" href="<?= types_render_field('stock-link', array('output'=>'raw')) ?>" class="footer-link">Подробнее</a>
					</div>
				</div>
			</div>
		<?php endwhile; else: ?>
		<p><?php _e('Записей пока нет...'); ?></p>
		<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>