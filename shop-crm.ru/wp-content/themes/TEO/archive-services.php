<?php get_header(); ?>
<section class="services js-check" data-underline="services-f" id="services">
	<div class="container text-center">
		<h2 class="title">Услуги</h2>
		<hr class="under-title-s">
		<p class="desc">С 2011 года мы помогаем компаниям разрабатывать и внедрять программное обеспечение, которое выводит бизнес на новый качественный уровень</p>
		<div class="wrapper">
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<?php $description = types_render_field("serv_desc", array('display'=>'raw')); ?>
			<div class="item col-md-4 col-sm-6 col-xs-12">
				<div class="item-link">
					<div class="item-body">
						<div class="icon-box" style="border-color: <?= types_render_field("icon-colorr", array('display'=>'raw')) ?>">
							<i
							style="color: <?= types_render_field("icon-colorr", array('display'=>'raw')) ?>"
							class="<?= types_render_field("icon-clas", array('display'=>'raw')) ?>"></i>
						</div>
						<h4 class="item-title"><?= the_title() ?></h4>
						<p class="item-desc"><?php
							if(strlen($description) > 115) {
								 echo mb_substr($description, 0, 110) . '...';
							} else {
								echo $description;
							}
						?></p>
					</div>
					<div class="item-footer">
						<a href="<?= the_permalink() ?>" rel="nofollow" class="footer-link">Подробнее</a>
					</div>
				</div>
			</div>
		<?php endwhile; else: ?>
		<p><?php _e('Записей пока нет...'); ?></p>
		<?php endif; ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
