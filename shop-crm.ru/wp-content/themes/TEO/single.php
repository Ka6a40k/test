<?php 

if( have_posts() ) { the_post(); rewind_posts(); }
	if( get_post_type() == 'portfolio') {
		get_template_part('layouts/portfolio-single');
	} 
	elseif( get_post_type() == 'blog' ) {
		get_template_part('layouts/blog-single');
	}
	elseif( get_post_type() == 'services' ) {
		get_template_part('layouts/service-single');
	} else {
		exit('404');
	}