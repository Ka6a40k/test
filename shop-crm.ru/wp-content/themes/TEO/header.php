<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>Technology To Every One</title>
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale = 1.0, user-scalable=yes">
	<meta name="HandheldFriendly" content="True">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="geo.placename" content="Новосибирск, Новосибирская обл., Россия" />
  <meta name="geo.position" content="55.0083530;82.9357330" />
  <meta name="geo.region" content="RU" />
  <meta name="ICBM" content="55.0083530, 82.9357330" />
	<link rel="shortcut icon" href="<?=get_template_directory_uri(); ?>/img/logo.ico" type="image/x-icon">
	<?php wp_head(); ?>
</head>
<body>
<!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter41905734 = new Ya.Metrika({ id:41905734, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/41905734" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
	<div id="fade-menu"></div>
	<div id="feedback-popup">
		<div class="col-xs-12 feedback-box">
			<a href="" id="close-feedback" onclick="hideFeedback(event)" ><i class="fa fa-times" aria-hidden="true"></i></a>
			<form action="<?= get_template_directory_uri() ?>/feedback.php" class="popup-form" onsubmit="send_PopupForm(event, $(this))">
				<h3 class="title">Получить <span>консультацию</span></h3>
				<div class="alert alert-success" id="alertPopup" role="alert"></div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
					<input id="name-p" type="text" class="form-control" name="name" placeholder="Имя">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
					<input id="email-p" type="text" class="form-control" name="email" placeholder="Email">
				</div>
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
					<input id="phone-p" type="text" class="form-control" name="phone" placeholder="Телефон">
				</div>
				<p class="agreement-alert">Нажимая на кнопку «Отправить», вы даете согласие на обработку персональных данных</p>
				<button type="submit" class="send-form">Отправить</button>
			</form>
			<p class="title hide" id="send_alert">Отправлено!</p>
		</div>
	</div>
	<section class="navigate-sec">
		<div class="contacts-bar">
			<div class="container">
				<div style="float: right;">
					<a href="tel:+79612206659" class="phone contact"><i class="tel glyphicon glyphicon-earphone"></i>+7(961)220-66-59</a>
					<a href="mailto:support@shop-crm.ru" class="email contact"><i class="glyphicon glyphicon-envelope"></i>support@shop-crm.ru</a>
				</div>
		  </div>
		</div>
		<div class="navigate-menu" id="nav-menu">
			<div class="container">
				<div class="logo-box">
					<a href="<?= home_url() ?>" class="logo-link"><img src="<?=get_template_directory_uri(); ?>/img/logo.png" alt="" class="logo"></a>
				</div>
				<ul class="navigate-box">
					<li class="nav-links" id="main-f"><div class="active-item"></div><a href="<?= home_url() ?>" class="nav-item">Главная</a></li>
					<li class="nav-links" id="services-f"><div class="active-item"></div><div class="active-item"></div><a href="<?= home_url() . '/services' ?>" class="nav-item">Услуги</a></li>
					<li class="nav-links" id="portfolio-f"><div class="active-item"></div><a href="<?= home_url() . '/portfolio' ?>" class="nav-item">Портфолио</a></li>
					<li class="nav-links" id="shares-f"><div class="active-item"></div><a href="<?= home_url() . '/shares' ?>" class="nav-item">Акции от партнеров</a></li>
					<li class="nav-links" id="blog-f"><div class="active-item"></div><a href="<?= home_url() . '/blog' ?>" class="nav-item">Блог</a></li>
					<li class="nav-links" id="contacts-f"><div class="active-item"></div><a href="<?= home_url() . '/contacts' ?>" class="nav-item">Контакты</a></li>
				</ul>
				<button class="btn btn-default" onclick="toggleMenu()"><i class="fa fa-bars" aria-hidden="true"></i></button>
			</div>
		</div>
	</section>
	<section class="header">
		<div class="container text-center">
			<div class="text-box">
				<h1 class="title">Автоматизация вашего бизнеса</h1>
				<p class="desc"> Мы занимаемся разработкой CRM систем "под ключ" для вашего бизнеса за 10 дней, с гарантией результата от профессиональной команды программистов с опытом работы 5 лет.</p>
				<a href="" class="header-btn" onclick="showFeedback(event, 'Получить консультацию')">Получить консультацию</a>
			</div>
		</div>
	</section>
