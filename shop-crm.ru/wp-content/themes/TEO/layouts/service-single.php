<?php get_header() ?>
<?php
	$future_titleField =  types_render_field('future_title', array('output'=>'raw'));
	$future_titleArray = preg_split("/;/", $future_titleField, NULL, PREG_SPLIT_NO_EMPTY);

	$future_descField = types_render_field('future_desc', array('output'=>'raw'));
	$future_descArray = preg_split("/;/", $future_descField, NULL, PREG_SPLIT_NO_EMPTY);

	$future_iconClass_field = types_render_field('future_icon_class', array('output'=>'raw'));
	$future_iconClass_array = preg_split("/;/", $future_iconClass_field, NULL, PREG_SPLIT_NO_EMPTY);

	$future_iconColor_field = types_render_field('future_icon_color', array('output'=>'raw'));
	$future_iconColor_array = preg_split("/\s/", $future_iconColor_field, NULL, PREG_SPLIT_NO_EMPTY);

	$i = 0;
?>





<section class="service-single">
	<div class="container text-center">
		<h2 class="title"><?= the_title() ?></h2>
		<hr class="under-title">
		<div class="header-box">
			<div class="desc-box col-md-8 col-sm-8 col-xs-12"><?= types_render_field('serv_desc', array('output'=>'raw')) ?></div>
			<div class="img-box col-md-4 col-sm-4 col-xs-12">
				<img src="<?= types_render_field('serv_img', array('output'=>'raw')) ?>" alt="" class="serv-img">
			</div>
		</div>
	</div>
</section>
<section class="body-box">
	<div class="container text-center">
		<h2 class="title"><?= types_render_field("future_block_title", array('display'=>'raw'))?></h2>
		<hr class="under-title">
		<div class="wrapper">
			<?php foreach ($future_titleArray as $item ): ?>

				<div class="item col-md-4 col-sm-6 col-xs-12">
					<div class="item-box">
						<div class="icon-box" style="border-color: <?= $future_iconColor_array[$i] ?>">
							<i
							style="color: <?= $future_iconColor_array[$i] ?>"
							class="<?= $future_iconClass_array[$i] ?>"></i>
						</div>
						<h4 class="item-title"><?= $item ?></h4>
						<p class="item-desc"><?= $future_descArray[$i] ?></p>
					</div>
				</div>

			<?php $i++;   endforeach; ?>
		</div>
	</div>
</section>
<section class="serv-desc">
	<div class="container">
		<h2 class="title"><?= types_render_field("serv_desc_title", array('output'=>'raw')) ?></h2>
		<hr class="under-title">
		<div class="wrapper"><?= types_render_field("serv_desc_text", array()) ?></div>
		<div class="alert alert-success text-center notice">Хотите заказать эту услугу? Звоните по телефону <a href="tel:89612206659" class="link">+7(961)220-66-59</a> или <button class="label bg-success" onclick="showFeedback(event, 'Заказать услугу:', '<?= the_title() ?>')">НАПИШИТЕ НАМ</button> и мы решим поставленную задачу!</div>
	</div>
</section>
<section class="additional-posts">
	<div class="container">
		<h2 class="title">Остальные <span>услуги</span></h2>
		<hr class="under-title">
		<div class="wrapper">
			<?php
			$this_post = $post->ID;
			$args = array(
				'post_type' => 'services',
				'posts_per_page' => 10,
				'post__not_in' => array($this_post)
				);
			$query = new WP_Query($args);
			while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="service-item col-md-4 col-sm-6 col-xs-12">
					<a href="<?=the_permalink()?>" class="item-link">
						<div class="icon-box"><i
						style="color: <?= types_render_field("icon-colorr", array('display'=>'raw')) ?>"
						class="<?= types_render_field("icon-clas", array('display'=>'raw')) ?>"></i></div>
						<div class="text-box"><?= the_title(); ?></div>
					</a>
				</div>

			<?php endwhile; ?>
		</div>
	</div>
</section>

<?php get_footer() ?>
