<?php get_header() ?>
<?php 

$imgs = types_render_field("image", array('output'=>'raw'));
$imgArray = preg_split("/\s/", $imgs, NULL, PREG_SPLIT_NO_EMPTY);
?>
<section class="single-blog-sec">
	<div class="container text-center">
		<h2 class="title"><?= the_title() ?></h2>
		<hr class="under-title-s">
		<div id="single-blog-slide">
			<div class="slide-item">
				<?php the_post_thumbnail( 'full', array('class' => "slide-img")) ?>
			</div>
			<?php foreach ($imgArray as $img): ?>
				<?php if($img !== ''): ?>
										
					<div class="slide-item">
						<img src="<?= $img ?>" alt="" class="slide-img">
					</div>
					
				<?php endif; ?>
			<?php endforeach; ?>
		</div>
		<div class="wrapper">
			<?php echo types_render_field('text1', array()) ?>
		</div>
	</div>
</section>
<section class="additional-posts">
	<div class="container">
		<h2 class="title">Соседние <span>статьи</span></h2>
		<hr class="under-title">
		<div class="wrapper">
			<?php 
			$this_post = $post->ID;
			$args = array(
				'post_type' => 'blog',
				'posts_per_page' => 4,
				'post__not_in' => array($this_post)
				);
			$query = new WP_Query($args);
			while ( $query->have_posts() ) : $query->the_post(); ?>

				<div class="item col-md-6 col-sm-6 col-xs-12">
					<a href="<?= the_permalink(); ?>" class="item-link">
						<div class="img-box pull-left">
							<?php the_post_thumbnail( 'thumbnail', array('class' => "item-img")) ?>
						</div>
						<div class="text-box pull-left">
							<p class="title"> <?= the_title() ?></p>
						</div>
					</a>
				</div>

			<?php endwhile; ?>
		</div>
	</div>
</section>

<?php get_footer() ?>