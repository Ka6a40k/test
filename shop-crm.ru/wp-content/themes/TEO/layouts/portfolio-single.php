<?php get_header(); ?>

<?php
$field = types_render_field("desc", array('row'=>'true'));

$fieldArray = preg_split("/;/", $field, NULL, PREG_SPLIT_NO_EMPTY);


$imgField = types_render_field("img", array('output'=>'raw'));
$imgArray = preg_split("/\s/", $imgField, NULL, PREG_SPLIT_NO_EMPTY);

?>

<section class="portf-header">
	<div class="container text-center">
		<h2 class="title"><?= the_title() ?></h2>
		<hr class="under-title">
		<div class="wrapper">
			<div id="singleCarousel" class="col-md-6 col-sm-6 col-xs-12">
				<div class="item">
					<?php the_post_thumbnail( 'full', array('class' => "item-img")) ?>
				</div>
				<?php foreach ($imgArray as $img): ?>
					<?php if($img !== ''): ?>

						<div class="item">
							<img src="<?= $img ?>" alt="" class="item-img">
						</div>

					<?php endif; ?>
				<?php endforeach;	?>
			</div>
			<div class="text-box col-md-6 col-sm-6 col-xs-12">
				<p class="short-desc"><span>Цена: </span> <?= types_render_field('pricee', array('output'=>'raw')) ?></p>
				<p class="short-desc"><span>Сроки: </span><?= types_render_field('date', array('output'=>'raw')) ?></p>
				<div class="desc-box"><?php
					$short_title = types_render_field('short_title', array('output'=>'raw'));
					if (strlen($short_title) > 0){ ?>
						<h4 class="title"><?= $short_title ?></h4>
					<?php }?>
					<p class="short-desc"><?= types_render_field('short_desc', array()) ?></p>
				</div>
				<button class="btn-order-test col-md-12 col-sm-12 col-xs-12" onclick="showFeedback(event, 'Протестировать систему:', '<?= the_title()?>')">Протестировать систему</button>
			</div>
		</div>
	</div>
</section>
<section class="portf-clients">
	<div class="container">
		<h2 class="title">Кому подойдет данная система?</h2>
		<hr class="under-title">
		<div class="wrapper">
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<div class="item-box">
					<div class="img-box">
						<img src="<?= types_render_field('box_img_1', array('output'=>'raw')) ?>" alt="" class="item-img">
					</div>
					<h5 class="title"><?= types_render_field('box_desc_1', array('output'=>'raw')) ?></h5>
				</div>
			</div>
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<div class="item-box">
					<div class="img-box">
						<img src="<?= types_render_field('box_img_2', array('output'=>'raw')) ?>" alt="" class="item-img">
					</div>
					<h5 class="title"><?= types_render_field('box_desc_2', array('output'=>'raw')) ?></h5>
				</div>
			</div>
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<div class="item-box">
					<div class="img-box">
						<img src="<?= types_render_field('box_img_3', array('output'=>'raw')) ?>" alt="" class="item-img">
					</div>
					<h5 class="title"><?= types_render_field('box_desc_3', array('output'=>'raw')) ?></h5>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="portf-desc">
	<div class="container">
		<h2 class="title"> <?= types_render_field('portf_body_title', array('output'=>'raw')) ?></h2>
		<hr class="under-title">
		<div class="desc-box"><?= types_render_field('full_desc', array()) ?></div>
		<button class="btn-order-test col-md-4 col-sm-8 col-xs-12" onclick="showFeedback(event, 'Протестировать систему:', '<?= the_title()?>')">Протестировать систему</button>
	</div>
</section>
<?php get_footer(); ?>
