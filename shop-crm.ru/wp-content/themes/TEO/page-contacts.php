<?php
/*
Template Name: contacts
*/
?>
<?php get_header() ?>

<section class="contacts-sec js-check" id="contacts" data-underline="contacts-f">
	<div class="container text-center">
		<h2 class="title">Контакты</h2>
		<div class="under-title-s"></div>
		<div class="wrapper">
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<a class="box" href="tel:89612206659">
					<div class="icon-box"><i class="fa fa-phone" aria-hidden="true"></i></div>
					<div class="text-box">+7(961)220-66-59</div>
				</a>
			</div>
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<a class="box" href="mailto:support@shop-crm.ru" target="_blank">
					<div class="icon-box"><i class="fa fa-envelope" aria-hidden="true"></i></div>
					<div class="text-box">support@shop-crm.ru</div>
				</a>
			</div>
			<div class="item col-md-4 col-sm-4 col-xs-12">
				<a href="https://vk.com/teo_technology_to_everyone" target="_blank" class="box">
					<div class="icon-box"><i class="fa fa-vk" aria-hidden="true"></i></div>
					<div class="text-box">Группа в ВК</div>
				</a>
			</div>
		</div>
		<h2 class="title">Обратная связь</h2>
		<div class="under-title-s"></div>
		<form action="<?= get_template_directory_uri() ?>/feedback.php" class="feedback-form text-left" onsubmit="send_form(event, $(this))">
			<div class="item col-md-4 col-sm-4 col-xs-12">
			 	<div class="form-group">
					<label for="name-f">Имя</label>
					<input type="text" id="name-f" class="form-input" required placeholder="Артем">
				</div>
				<div class="form-group">
					<label for="email-f">Email</label>
					<input type="text" id="email-f" class="form-input" required placeholder="support@shop-crm.ru">
				</div>
				<div class="form-group">
					<label for="phone-f">Телефон</label>
					<input type="text" id="phone-f" class="form-input" required placeholder="+7(961)220-66-59">
				</div>
			</div>
			<div class="item col-md-8 col-sm-8 col-xs-12">
				<label for="message-f">Сообщение</label>
				<textarea name="" id="message-f" class="form-textarea"></textarea>
			</div>
			<div class="form-footer">
				<div class="item col-md-4 col-sm-4 col-xs-12">
					<p class="text-center">Нажимая на кнопку «Отправить», вы даете согласие на обработку персональных данных</p>
					<button class="btn-send"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>ОТПРАВИТЬ</button>
				</div>
			</div>
		</form>
	</div>
</section>

<?php get_footer() ?>